import babel from "rollup-plugin-babel";
import commonjs from "rollup-plugin-commonjs";
import resolve from "rollup-plugin-node-resolve";
import { uglify } from "rollup-plugin-uglify";
import replace from "rollup-plugin-replace";
import json from "rollup-plugin-json";

const env = process.env.NODE_ENV;

const plugins = [
  json({
    indent: '  ',
    compact: true, 
    namedExports: false 
  }),
  replace({
    "process.env.NODE_ENV": JSON.stringify(env)
  }),
  babel({
    exclude: "node_modules/**",
    plugins: ["external-helpers"]
  }),
  resolve({
    browser: true
  }),
  commonjs({
    include: ["node_modules/**"],
    exclude: ["node_modules/process-es6/**"],
    namedExports: {
      "node_modules/react/index.js": [
        "Children",
        "Component",
        "PropTypes",
        "createElement"
      ],
      "node_modules/react-dom/index.js": ["render"],
      "node_modules/react-is/index.js": ["isValidElementType"]
    }
  })
];

if (env === "production") {
  plugins.push(
    uglify({
      compress: true
    })
  );
}

export default {
  input: "./src/index.js",
  output: {
    file: "dist/bundle.js",
    format: "umd"
  },
  plugins: plugins
};