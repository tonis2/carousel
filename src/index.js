import ReactDOM from "react-dom";
import React from "react";
import Carousel from "./components/carousel";
import data from "./data.json";


ReactDOM.render(
    <Carousel message={data[0]}></Carousel>,
    document.getElementById('root')
  );