import React from "react";

class Carousel extends React.Component {
  constructor() {
    super();
    this.state = {
      scrollWidth: 0,
      currentPage: 0
    }
    this.pages = 0;
    this.pageWidth = 300;
    this.scrollLeft = this.scrollLeft.bind(this);
    this.scrollRight = this.scrollRight.bind(this);
    this.createIndicator = this.createIndicator.bind(this);
  }

  scrollLeft() {
    const page = this.state.currentPage - 1
    if (page === -1) {
      this.state.currentPage = 0;
      return
    }
    this.setState({
      currentPage: page,
      scrollWidth: page * -this.pageWidth
    })
  }

  scrollRight() {
    const page = this.state.currentPage + 1
    if (page === this.pages) {
      this.setState({ 
        currentPage: 0,
        scrollWidth: 0
      })
      return
    }
    this.setState({
      currentPage: page,
      scrollWidth: page * -this.pageWidth
    })
  }

  createIndicator(length) {
    const indicators = [];
    for (let i = 0; i < length; i++) {
      const indicator = <circle cx={10 + i * 25} cy="10" r="6" fill={this.state.currentPage === i ? "grey" : "rgba(46, 44, 44, 0.342)"} />
      indicators.push(indicator)
    }
    return <svg height="20" width={`${length * 25}px`} id="carousel-pages-indicator">{indicators}</svg>;
  }

  render() {
    const { carousel } = this.props.message;
    this.pages = carousel.length;
    console.log(carousel)
    return (
      <section id="carousel-container" style={{ "--carousel-lenght": carousel.length, "--carousel-width": `${this.pageWidth}px` }}>
        <div onClick={this.scrollLeft} className="carousel-arrow-selection left">&#60;</div>
        <div onClick={this.scrollRight} className="carousel-arrow-selection right">&#62;</div>
        {this.createIndicator(carousel.length)}
        <div id="carousel-content" style={{ "transform": `translateX(${this.state.scrollWidth}px)` }}>
          {
            carousel.map((item, key) => {
              return <section key={key} className="carousel-content-item" >
                <img className="carousel-item-image" src={item.image_url} alt="image" />
                <section className="carousel-item-text-content">
                  <div className="carousel-item-title"><span>{item.title}</span></div>
                  <div className="carousel-item-info"><p>{item.subtitle}</p></div>
                </section>
                <section id="buy-buttons">
                  <a target="_blank" href={item.button[0].url}>{item.button[0].title}</a>
                  <a target="_blank" href={item.button[1].url}>{item.button[1].title}</a>
                </section>
              </section>
            })
          }
        </div>
      </section>
    )
  }
}

export default Carousel;
